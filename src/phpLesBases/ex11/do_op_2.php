<?php

if ($argc != 2) {
    echo "Incorrect Parameters\n";
    exit;
} else {
    $argv1 = $argv[1];
    // preg replace enlève les espaces
    $sanses = preg_replace("/\s+/", '', $argv1);
    $tab = trim($sanses);
    // preg split va enlever tout avant et après la regex
    $bla = preg_split('/\+|\*|\/|\-|\%/', $tab);
    // preg match va récuper la valeur regex et la mettre dans la variable qui renvoi true ou false
    $ope = preg_match('/\+|\*|\/|\-|\%/', $tab, $matches);
    // s'il y a matches
    if ($ope) {
        $bla1 = $matches[0];
    } else {
        echo "Syntax Error\n";
        exit;
    }
    $bla0 = $bla[0];
    $bla2 = $bla[1];
    // test si numérique
    if (!is_numeric($bla0) || !is_numeric($bla2)) {
        echo "Syntax Error\n";
        exit;
    } else {
        // test les opérateurs
        if ($bla1 != '+' && $bla1 != '*' && $bla1 != '-' && $bla1 != '%' && $bla1 != '/') {
            echo "Incorrect Parameters\n";
            exit;
        } else {
            if ($bla1 == '/' && $bla2 == 0 && $argv1 == 0) {
                echo "0\n";
            } else {
                if ($bla1 == '+') {
                    echo $bla0 + $bla2 . "\n";
                } else {
                    if ($bla1 == '-') {
                        echo $bla0 - $bla2 . "\n";
                    } else {
                        if ($bla1 == '*') {
                            echo $bla0 * $bla2 . "\n";
                        } else {
                            if ($bla1 == '%') {
                                echo $bla0 % $bla2 . "\n";
                            } else {
                                if ($bla1 == '/') {
                                    echo $bla0 / $bla2 . "\n";
                                } else {
                                    echo "Incorrect Parameters\n";
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
