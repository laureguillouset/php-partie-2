<?php

$content = file_get_contents($argv[1]);
$change = preg_replace_callback(
    // on vise le title=
        '/title="(.*)"/',
        function ($matches) {
            return 'title=' . '"' . strtoupper($matches[1]) . '"';
        }, $content);

$change = preg_replace_callback(
    // On vise la ligne <a href
        '/<a [^>]+.*<\/a>/U',
        function ($matches) {
            return preg_replace_callback(
                // on vise le >< dans le <a href
                '/>.*</U', function ($matches) {
                    return strtoupper($matches[0]);
                }, $matches[0]);
        }, $change);

echo $change;
