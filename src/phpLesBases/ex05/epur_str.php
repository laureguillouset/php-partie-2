<?php

// si $argc= nombre de paramètres est super rieu à 2 OU !isset= est nul le tableau argv1
if ($argc > 2 || !isset($argv[1])) {
    // on sort
    exit();
}
$input = $argv[1];
// trim = pas d'espace au début et fin; preg_replace= remplacer les espace vide par des espaces
$output = trim(preg_replace('/[[:blank:]]+/', ' ', $input));

echo $output . "\n";
