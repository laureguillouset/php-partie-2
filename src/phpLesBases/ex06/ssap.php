<?php

// crér une grande chaine de caractère avec des espaces pour séparateur; array_slice= prendre le tableau à partir de l'index indiqué
$str = implode(' ', array_slice($argv, 1));

// Toujours créer une variable pour encadréer le implode ou explode
$var = explode(' ', $str);

sort($var);

// Permet d'afficher le tableau un à un
foreach ($var as $key => $value) {
    echo $value . "\n";
}
